wmbutton (0.7.1-2) UNRELEASED; urgency=medium

  * Update Vcs-* after migration to Salsa.
  * Bump Standards-Version to 4.1.3.

 -- Doug Torrance <dtorrance@piedmont.edu>  Wed, 10 Jan 2018 14:45:50 -0500

wmbutton (0.7.1-1) unstable; urgency=medium

  * New maintainer (Closes: #866847).
  * New upstream release.
  * debian/compat
    - Bump debhelper compatibility level to 10.
  * debian/control
    - Bump Standards-Version to 4.0.0.
    - Bump debhelper to >= 10 in Build-Depends.
    - Remove x11proto-core-dev from Build-Depends.  It was redundant
      as it is already a dependency of libx11-dev, libxext-dev, and
      libxpm-dev.
    - Add pkg-config to Build-Depends.
    - Update Homepage and Vcs-*.
    - Sort with wrap-and-sort.
  * debian/copyright
    - Update Format.
  * debian/docs
    - Do not install example config file; it is exactly the same
      as the system config file installed in /etc.
  * debian/manpages
    - Remove unnecessary file; manpage already installed by upstream
      build system.
  * debian/menu
    - Remove file; the Debian menu has been deprecated.
  * debian/patches/01_Debian_config.patch
    - Refresh for new version.
  * debian/README.Debian
    - Remove file; its contents were not Debian-specific and were
      already covered in the upstream README.
  * debian/rules
    - Use all hardening flags.
    - Remove CPPFLAGS hack; no longer needed.
  * debian/watch
    - Update with new download location.
  * debian/wmbutton.lintian-overides
    - Remove file.  The only override was no-upstream-changelog, but
      upstream now ships a changelog.

 -- Doug Torrance <dtorrance@piedmont.edu>  Mon, 17 Jul 2017 05:07:36 -0400

wmbutton (0.7.0-2) unstable; urgency=low

  * Package moved from experimental to unstable and updated.
  * debian/control
    - Bumped Standards-Version to 3.9.4
    - Removed DM-Upload-Allowed flag.
    - Maintainer email address changed from kix@kix.es to kix@debian.org.
    - Added Vcs-Git and Vcs-Browser fields.
    - Debconf version 9 (previous was 8.1.3~).
  * Removed debian/source/lintian-overrides
    - Because debhelper is version 9, the lintian override
      "package-needs-versioned-debhelper-build-depends 9" is not needed.

 -- Rodolfo García Peñas (kix) <kix@debian.org>  Sun, 21 Jul 2013 10:44:11 +0200

wmbutton (0.7.0-1) experimental; urgency=low

  * New maintainer (Closes: #661120)
  * New upstream version (Closes: #632477).
  * debian/control:
    - Bumped Standards-Version to 3.9.3
    - Debconf version 9 (see debian/compat too).
    - Added info about wmaker package in description
    - New Homepage field.
    - Included DM-Upload-Allowed.
    - Removed priority and section fields in binary package.
  * debian/rules:
    - File full rewrited.
  * debian/manpages: manpage is now at upstream.
  * New debian/patches (DEP-3).
    - 01_Debian_config.patch; Include:
      - Debian specific icons.
      - Debian specific config file.
  * debian/copyright is now DEP-5.
  * Added debian/watch file to avoid lintian problem.
    - This file has a comment because the upstream is a git repository.
  * debian/menu: Section changed to "Applications/File Management"
  * Added lintian-overides file because upstream changelog is not available.

 -- Rodolfo García Peñas (kix) <kix@kix.es>  Tue, 21 Aug 2012 21:32:55 +0200

wmbutton (0.6.1-3.1) unstable; urgency=low

  [Jari Aalto]
  * Non-maintainer upload.
  * debian/control
    - (Build-Depends): update obsolete x-dev to x11proto-core-dev.
      (important RC bug; Closes: #515404).

 -- Jari Aalto <jari.aalto@cante.net>  Sat, 27 Mar 2010 10:27:25 +0200

wmbutton (0.6.1-3) unstable; urgency=low

  * debian/control: Updated build-depends (removed xlibs-dev)
    (Closes: #346897)
  * debian/control: Standards-Version to 3.6.2

 -- Christian Aichinger <Greek0@gmx.net>  Fri,  6 Jan 2006 02:00:27 +0100

wmbutton (0.6.1-2) unstable; urgency=low

  * Fixed build problems on several platforms
  * debian/control: Fixed binary package section and priority

 -- Christian Aichinger <Greek0@gmx.net>  Tue, 15 Feb 2005 00:48:52 +0100

wmbutton (0.6.1-1) unstable; urgency=low

  * New Maintainer
  * New upstream version 0.6.1 (Closes: #245868)
  * debian/control: Standards-Version to 3.6.1.1

 -- Christian Aichinger <Greek0@gmx.net>  Sun, 16 Jan 2005 16:40:13 +0100

wmbutton (0.4-5) unstable; urgency=low

  * Update README.Debian to clarify customization (Closes: #179418)
  * debian/control: Standards-Version to 3.5.8
  * debian/rules: Add support for DEB_BUILD_OPTIONS
  * debian/conffiles: Remove /etc/wmbutton.conf, debhelper
                      adds it automatically

 -- Gordon Fraser <gordon@debian.org>  Fri,  7 Feb 2003 19:56:54 +0100

wmbutton (0.4-4) unstable; urgency=low

  * Applied patch from Steve Kemp <skx@tardis.ed.ac.uk> to avoid buffer
    overflow (Closes: #167225)

 -- Gordon Fraser <gordon@debian.org>  Thu, 31 Oct 2002 15:59:47 +0100

wmbutton (0.4-3) unstable; urgency=low

  * Manpage updates
  * Minor code cleanups, documentation updates
  * Update package description

 -- Gordon Fraser <gordon@debian.org>  Mon,  4 Feb 2002 17:50:43 +0100

wmbutton (0.4-2) unstable; urgency=low

  * Update manpage to credit author
  * Fix spelling mistakes (Closes: #125487)
  * Add information how to customize
  * Update maintainer's email

 -- Gordon Fraser <gordon@debian.org>  Wed,  5 Dec 2001 09:00:29 +0100

wmbutton (0.4-1) unstable; urgency=low

  * Initial Release - (Closes: #113946)

 -- Gordon Fraser <gordon@debian.org>  Tue, 18 Sep 2001 20:27:46 +0200
